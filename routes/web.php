<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
// use Illuminate\Routing\Route;

Route::get('/', function () {
    return view('welcome');
});


Route::resource('projects', 'ProjectController');
Route::resource('tasks', 'TaskController');
Route::put('tasks/{id}/done', 'TaskController@done')->name('tasks.done');

Auth::routes();

Route::get('/home', 'ProjectController@index')->name('home');

Auth::routes();

Route::get('/home', 'ProjectController@index')->name('home');
