@extends('layouts.app')

@section('content')
<div class="container">
    <h2 class="my-4">Create Project</h2>
    <hr>
    <a href="{{route('projects.index')}}" class="btn btn-danger">Kembali</a>
    <div class="row mt-5">
        <div class="col-md-6">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

            <form action="{{route('projects.store')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="nama">Nama project</label>
                    <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama projek">
                </div>
                <div class="form-group row">
                    <div class="col-6">
                        <label for="mulai">Tanggal dimulai</label>
                        <input type="date" name="mulai" id="mulai" class="form-control" placeholder="Dimulai pada">
                    </div>
                    <div class="col-6">
                        <label for="target">Target selesai</label>
                        <input type="date" name="target" id="target" class="form-control" placeholder="Target pada">
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-info">Tambah projek</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection