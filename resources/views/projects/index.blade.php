@extends('layouts.app')

@section('content')
<div class="container">
    <h2 class="my-4">Projects</h2>
    <hr>
    <a href="{{route('projects.create')}}" class="btn btn-info">Tambah Projek</a>
    <div class="row mt-5">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <th>No</th>
                        <th>Nama projek</th>
                        <th>Dimulai pada</th>
                        <th>Target selesai</th>
                        <th>Selesai pada</th>
                        <th>Terakhir diupdate</th>
                        <th>Aksi</th>
                    </thead>
                    <tbody>
                        @foreach ($projects as $project)
                            <tr>
                            <td>{{$loop->iteration}}</td>
                            <td><a href="{{route('projects.show', $project->id)}}">{{$project->nama}}</a></td>
                            <td>{{$project->tanggal_mulai}}</td>
                            <td>{{$project->tanggal_target}}</td>
                            <td>{{$project->tanggal_selesai}}</td>
                            <td>{{$project->updated_at}}</td>
                            <td>
                                <a href="{{route('projects.edit', $project->id)}}" class="m-1 btn btn-sm btn-success p-1">Sunting</a>
                                <form action="{{route('projects.destroy', $project->id)}}" method="post">
                                    @method('delete')
                                    @csrf
                                <button class="btn btn-danger btn-sm m-1">Hapus</button>
                                </form>
                            </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
@endsection