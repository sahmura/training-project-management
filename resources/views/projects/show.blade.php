@extends('layouts.app')

@section('content')
<div class="container">
    <h2 class="my-4">{{$project->nama}}</h2>
    <hr>
    <a href="{{route('projects.index')}}" class="btn btn-danger">Kembali</a>
    <a data-toggle="modal" data-target="#exampleModal" class="btn btn-info">Tambah task</a>
    <div class="row mt-5">
        <div class="col-md-12">
            <h6 class="mx-2">List tasks</h6>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <th>No</th>
                        <th>Nama task</th>
                        <th>Keterangan</th>
                        <th>Target selesai</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </thead>
                    <tbody>
                        @foreach ($tasks as $task)
                        <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$task->nama}}</td>
                        <td>{{$task->keterangan}}</td>
                        <td>{{$task->tanggal_target}}</td>
                        <td>{{$task->done}}</td>
                        <td>
                            <a href="{{route('tasks.edit', $task->id)}}" class="m-1 btn btn-sm btn-block btn-success p-1">Sunting</a>
                            <form action="{{route('tasks.done', $task->id)}}" method="post">
                                @method('put')
                                @csrf
                                <input type="text" name="id" value="{{$task->project_id}}" hidden>
                            <button class="btn btn-info btn-sm m-1 btn-block">Selesai</button>
                            </form>
                                <form action="{{route('tasks.destroy', $task->id)}}" method="post">
                                    @method('delete')
                                    @csrf
                                <button class="btn btn-danger btn-sm m-1 btn-block">Hapus</button>
                                </form>
                        </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah task</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('tasks.store')}}" method="post">
                @csrf
                <div class="form-group">
                    <input type="text" name="id" id="id" class="form-control" placeholder="Nama task" value="<?php echo $project->id; ?>" hidden>
                </div>
                <div class="form-group">
                    <label for="nama">Nama task</label>
                    <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama task">
                </div>
                <div class="form-group">
                    <label for="keterangan">Keterangan</label>
                    <textarea type="text" name="keterangan" id="keterangan" class="form-control" placeholder="Keterangan" rows="3"></textarea>
                </div>

                <div class="form-group row">
                    <div class="col-6">
                        <label for="mulai">Tanggal dimulai</label>
                        <input type="date" name="mulai" id="mulai" class="form-control datepicker" placeholder="Dimulai pada">
                    </div>
                    <div class="col-6">
                        <label for="target">Target selesai</label>
                        <input type="date" name="target" id="target" class="form-control datepicker" placeholder="Target pada">
                    </div>
                </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Tambah</button>
      </form>
      </div>
    </div>
  </div>
</div>