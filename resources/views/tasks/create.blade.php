@extends('layout')

@section('content')
    <h2 class="my-4">Tambah Task</h2>
    <hr>
    <a href="{{url()->previous()}}" class="btn btn-danger">Kembali</a>
    <div class="row mt-5">
        <div class="col-md-6">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

            <form action="{{route('tasks.store')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="nama">Nama task</label>
                    <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama task">
                </div>
                <div class="form-group">
                    <label for="nama">Nama task</label>
                    <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama task">
                </div>
                <div class="form-group">
                    <label for="keterangan">Keterangan</label>
                    <textarea type="text" name="keterangan" id="keterangan" class="form-control" placeholder="Keterangan" rows="3"></textarea>
                </div>

                <div class="form-group row">
                    <div class="col-6">
                        <label for="mulai">Tanggal dimulai</label>
                        <input type="date" name="mulai" id="mulai" class="form-control datepicker" placeholder="Dimulai pada">
                    </div>
                    <div class="col-6">
                        <label for="target">Target selesai</label>
                        <input type="date" name="target" id="target" class="form-control datepicker" placeholder="Target pada">
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-info">Tambah task</button>
                </div>
            </form>
        </div>
    </div>

@endsection