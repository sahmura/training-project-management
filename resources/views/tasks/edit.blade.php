@extends('layouts.app')

@section('content')
<div class="container">
    <h2 class="my-4">Update Task</h2>
    <hr>
    <a href="{{url()->previous()}}" class="btn btn-danger">Kembali</a>
    <div class="row mt-5">
        <div class="col-md-6">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

            <form action="{{route('tasks.update', $task->id)}}" method="post">
                @method('put')
                @csrf
                <div class="form-group">
                    <input type="text" name="id" id="id" class="form-control" placeholder="Nama task" value="<?php echo $task->project_id; ?>" hidden>
                </div>
                <div class="form-group">
                    <label for="nama">Nama Task</label>
                <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama projek"  value="{{$task->nama}}">
                </div>
                <div class="form-group">
                    <label for="keterangan">Keterangan</label>
                <textarea type="text" name="keterangan" id="keterangan" class="form-control" placeholder="Keterangan" rows="3">{{$task->keterangan}}</textarea>
                </div>
                <div class="form-group row">
                    <div class="col-6">
                        <label for="mulai">Tanggal dimulai</label>
                        <input type="date" name="mulai" id="mulai" class="form-control datepicker" placeholder="Dimulai pada" value="{{$task->tanggal_mulai}}">
                    </div>
                    <div class="col-6">
                        <label for="target">Target selesai</label>
                        <input type="date" name="target" id="target" class="form-control datepicker" placeholder="Target pada" value="{{$task->tanggal_target}}">
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-info">Update projek</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection