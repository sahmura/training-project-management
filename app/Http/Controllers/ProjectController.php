<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Project;
use \App\Task;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{
    public function index(Request $req)
    {
        $user = Auth::user();
        $projects = Project::where('id_user', $user->id)->orderBy('id', 'desc')->get();
        return view('projects.index', compact('projects'));
    }

    public function create()
    {
        return view('projects.create');
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        $validasi = $request->validate([
            'nama' => 'required|max:255',
            'mulai' => 'required',
            'target' => 'required',
        ]);


        $nama = $request['nama'];
        $mulai = $request['mulai'];
        $target = $request['target'];

        $project = new Project();
        $project->id_user = $user->id;
        $project->nama = $nama;
        $project->tanggal_mulai = $mulai;
        $project->tanggal_target = $target;
        $project->created_at = now();
        $project->updated_at = now();
        $project->save();

        return redirect('projects');
    }

    public function show($id)
    {
        $project = Project::find($id);
        $user = Auth::user();

        if ($project->id_user != $user->id) {
            return redirect('projects');
        }

        $tasks = Task::where('project_id', $id)->get();
        return view('projects.show', compact('project', 'tasks'));
    }

    public function edit($id)
    {
        $project = Project::find($id);

        $user = Auth::user();

        if ($project->id_user != $user->id) {
            return redirect('projects');
        }

        return view('projects.edit', compact('project'));
    }

    public function update(Request $request, $id)
    {
        $project = Project::find($id);
        $validasi = $request->validate([
            'nama' => 'required|max:255',
            'mulai' => 'required',
            'target' => 'required',
        ]);

        $project->nama = $request->nama;
        $project->tanggal_mulai = $request->mulai;
        $project->tanggal_target = $request->target;
        $project->updated_at = now();
        $project->save();

        return redirect('projects');
    }

    public function destroy($id)
    {
        $project = Project::find($id);

        $user = Auth::user();

        if ($project->id_user != $user->id) {
            return redirect('projects');
        }

        $task = Task::where('project_id', $id)->delete();

        $project->delete();

        return redirect('projects');
    }
}
