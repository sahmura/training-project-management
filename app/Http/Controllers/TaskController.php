<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use \App\Project;
use \App\Task;

class TaskController extends Controller
{
    // public function index()
    // {
    //     $projects = Project::orderBy('id', 'desc')->get();
    //     return view('projects.index', compact('projects'));
    // }

    public function create()
    {
        return view('tasks.create');
    }

    public function store(Request $request)
    {
        $validasi = $request->validate([
            'nama' => 'required|max:255',
            'mulai' => 'required',
            'target' => 'required',
            'id' => 'required',
            'keterangan' => 'required',
        ]);

        $nama = $request['nama'];
        $mulai = $request['mulai'];
        $target = $request['target'];
        $keterangan = $request['keterangan'];
        $id = $request['id'];

        $task = new Task();
        $task->nama = $nama;
        $task->keterangan = $keterangan;
        $task->done = 0;
        $task->tanggal_mulai = $mulai;
        $task->tanggal_target = $target;
        $task->tanggal_selesai = null;
        $task->project_id = $id;
        $task->created_at = now();
        $task->updated_at = now();
        $task->save();

        return redirect('projects/' . $id);
    }

    public function edit($id)
    {
        $task = Task::find($id);
        return view('tasks.edit', compact('task'));
    }

    public function update(Request $request, $id)
    {
        $task = Task::find($id);
        $validasi = $request->validate([
            'nama' => 'required|max:255',
            'keterangan' => 'required',
            'mulai' => 'required',
            'target' => 'required',
        ]);

        $task->nama = $request->nama;
        $task->keterangan = $request->keterangan;
        $task->tanggal_mulai = $request->mulai;
        $task->tanggal_target = $request->target;
        $task->updated_at = now();
        $task->save();

        return redirect('projects/' . $request->id);
    }

    public function destroy($id)
    {
        $task = Task::find($id);
        $task->delete();

        return redirect('projects/' . $task->project_id);
    }

    public function done(Request $request, $id)
    {
        $task = Task::find($id);
        $task->done = 1;
        $task->updated_at = now();
        $task->save();

        return redirect('projects/' . $request->id);
    }
}
